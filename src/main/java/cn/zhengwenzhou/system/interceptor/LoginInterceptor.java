package cn.zhengwenzhou.system.interceptor;

import cn.zhengwenzhou.system.common.Constant;
import cn.zhengwenzhou.system.entity.User;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/22 15:48
 * @description 登录认证的拦截器
 */

public class LoginInterceptor implements HandlerInterceptor
{
	
	/**
	 * Handler执行完成之后调用这个方法
	 */
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exc) throws Exception
	{
		
	}
	
	/**
	 * Handler执行之后，ModelAndView返回之前调用这个方法
	 */
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
	{
	}
	
	/**
	 * Handler执行之前调用这个方法
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		
		String host = request.getRemoteHost();
		
		//获取请求的URL
		String url = request.getRequestURI();
		
		//获取Session
		HttpSession session = request.getSession();
		User currentUser = (User) session.getAttribute(Constant.SESSION_USER);
		
		if(currentUser != null)
		{
			return true;
		} else
		{
			response.sendRedirect("logout.do");
			return false;
		}
		
		
	}
}