package cn.zhengwenzhou.system.controller;

import cn.zhengwenzhou.system.common.Constant;
import cn.zhengwenzhou.system.entity.User;
import cn.zhengwenzhou.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/22 15:31
 * @description
 */

@Controller
public class LoginController
{
	
	@Autowired
	private UserService userServiceImpl;
	
	/**
	 * 登录
	 *
	 * @param session  HttpSession
	 * @param username 用户名
	 * @param password 密码
	 * @return
	 */
	@RequestMapping(value = "/login.do")
	public String login(HttpSession session, @RequestParam(name = "username", defaultValue = "") String username, @RequestParam(name = "password", defaultValue = "") String password)
	{
		User user = userServiceImpl.getUserByUsername(username);
		//TODO 判断帐号是否锁定
		if(user != null && user.getPassword().equals(password))
		{
			session.setAttribute(Constant.SESSION_USER, user);
			return "index.jsp";
		} else
		{
			return "login.jsp";
		}
	}
	
	/**
	 * 注销
	 *
	 * @param session Session
	 * @return
	 */
	@RequestMapping(value = "/logout.do")
	public String logout(HttpSession session)
	{
		//清除Session
		session.invalidate();
		
		return "login.jsp";
	}
}
