package cn.zhengwenzhou.system.controller;

import cn.zhengwenzhou.system.annotation.SysLog;
import cn.zhengwenzhou.system.entity.User;
import cn.zhengwenzhou.system.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/23 02:56
 * @description
 */
@Controller
@RequestMapping("/user")
public class UserController
{
	@Resource
	private UserService userServiceImpl;
	
	@RequestMapping("/get.do")
	@SysLog(module = "用户管理",methods = "查找用户")
	public String get(@RequestParam(name = "id",defaultValue = "0",required = false) long id)
	{
		User user = userServiceImpl.getUserById(id);
		System.out.println(user.toString());
		return "index.jsp";
	}
	
	@RequestMapping("/find.do")
	public String find(ModelAndView modelAndView)
	{
		User user = userServiceImpl.getUserById(1);
		System.out.println(user.toString());
		return "index.jsp";
	}
	
	@RequestMapping("/add.do")
	@SysLog(module = "用户管理",methods = "新增用户",description = "报错啦")
	public String addUser(ModelAndView modelAndView)
	{
		int a = 4 / 0;
		User user = new User();
		user.setUsername("admin");
		user.setPassword("123456");
		user.setNickname("管理员");
		userServiceImpl.insert(user);
		return "user.jsp";
	}
	
	@RequestMapping("/test.do")
	@SysLog(module = "用户管理" ,methods = "测试方法",description = "啦啦啦阿拉")
	public String test(ModelAndView modelAndView,int a,int b)
	{
		int c = 4/0;
		return "index.jsp";
	}
	
}
