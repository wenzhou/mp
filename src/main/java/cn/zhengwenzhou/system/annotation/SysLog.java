package cn.zhengwenzhou.system.annotation;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:59
 * @description 注解日志
 * 在Service层的方法名上面注解，例如：
 * @SysLog(module = "用户管理", methods = "添加用户")
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {
	
	//模块
	public String module() default "";
	//方法
	public String methods() default "";
	//描述
	public String description() default "";
	
}
