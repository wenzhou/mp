package cn.zhengwenzhou.system.service;

import cn.zhengwenzhou.system.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:57
 * @description 用户Service
 */
public interface UserService
{
	/**
	 * 获取所有用户
	 * @return List<User>
	 */
	public List<User> getAll();
	
	/**
	 * 根据分页获取用户
	 * @param pageIndex 起始条数
	 * @param pageSize 分页大小
	 * @return List<User>
	 */
	public List<User> getListByPage(int pageIndex,int pageSize);
	
	/**
	 * 根据ID获取用户
	 * @param id 用户ID
	 * @return User
	 */
	public User getUserById(long id);
	
	/**
	 * 根据用户名获取用户
	 * @param username 用户名
	 * @return User
	 */
	public User getUserByUsername(String username);
	
	/**
	 * 检测用户名是否存在
	 * @param username 用户名
	 * @return false：不存在，true：存在
	 */
	public boolean exist(String username);
	
	/**
	 * 获取总条数
	 * @return int
	 */
	public int count();
	
	/**
	 * 添加用户
	 * @param user 用户
	 */
	public void insert(User user);
	
	/**
	 * 更新用户
	 * @param user 用户
	 */
	public void update(User user);
	
	/**
	 * 真实删除用户(不可恢复)
	 * @param id 用户ID
	 */
	public void trueDelete(long id);
	
	/**
	 * 删除用户(更改用户状态，0：正常，1：锁定)
	 * @param id
	 */
	public void delete(long id);
	
	/**
	 * 批量删除用户
	 * @param ids 用户ID数组(List<Long>)
	 */
	public void batchDelete(List<Long> ids);
	
	/**
	 * 从Cookie获取用户ID
	 * @param request
	 * @return long
	 */
	public long getUserIdFromCookie(HttpServletRequest request);
	
	/**
	 * 从Cookie获取用户名
	 * @param request
	 * @return String
	 */
	public String getUsernameFromCookie(HttpServletRequest request);
	
	/**
	 * 从Session获取用户
	 * @param request
	 * @return User
	 */
	public User getSessionUser(HttpServletRequest request);
}
