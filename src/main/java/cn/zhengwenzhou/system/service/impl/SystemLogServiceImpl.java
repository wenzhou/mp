package cn.zhengwenzhou.system.service.impl;

import cn.zhengwenzhou.system.dao.SystemLogDao;
import cn.zhengwenzhou.system.entity.SystemLog;
import cn.zhengwenzhou.system.service.SystemLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:58
 * @description 用户Service实现
 */
@Service
public class SystemLogServiceImpl implements SystemLogService
{
	@Resource
	private SystemLogDao systemLogDaoImpl;
	
	@Override
	public List<SystemLog> getAll()
	{
		return systemLogDaoImpl.getAll();
	}
	
	@Override
	public List<SystemLog> getListByPage(int pageIndex, int pageSize)
	{
		Map<String, Integer> params = new HashMap<>();
		params.put("pageIndex", pageIndex);
		params.put("pageSize", pageSize);
		return systemLogDaoImpl.getListByPage(params);
	}
	
	@Override
	public SystemLog getSystemLogById(long id)
	{
		List<SystemLog> SystemLogs = systemLogDaoImpl.getSystemLogById(id);
		SystemLog SystemLog = null;
		if(SystemLogs.size() > 0)
		{
			SystemLog = SystemLogs.get(0);
		}
		return SystemLog;
	}
	
	@Override
	public int count()
	{
		return systemLogDaoImpl.count();
	}
	
	@Override
	public void insert(SystemLog systemLog)
	{
		systemLogDaoImpl.insert(systemLog);
	}
	
	@Override
	public void delete(long id)
	{
		systemLogDaoImpl.delete(id);
	}
	
	@Override
	public void batchDelete(List<Long> ids)
	{
		systemLogDaoImpl.batchDelete(ids);
	}
}
