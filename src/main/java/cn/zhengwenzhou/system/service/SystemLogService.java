package cn.zhengwenzhou.system.service;

import cn.zhengwenzhou.system.entity.SystemLog;

import java.util.List;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:57
 * @description 日志Service
 */
public interface SystemLogService
{
	/**
	 * 获取所有日志
	 * @return List<SystemLog>
	 */
	public List<SystemLog> getAll();
	
	/**
	 * 根据分页获取日志
	 * @param pageIndex 起始条数
	 * @param pageSize 分页大小
	 * @return List<SystemLog>
	 */
	public List<SystemLog> getListByPage(int pageIndex, int pageSize);
	
	/**
	 * 根据ID获取日志
	 * @param id 日志ID
	 * @return SystemLog
	 */
	public SystemLog getSystemLogById(long id);
	
	/**
	 * 获取总条数
	 * @return int
	 */
	public int count();
	
	/**
	 * 添加日志
	 * @param systemLog 日志
	 */
	public void insert(SystemLog systemLog);
	
	/**
	 * 删除日志(更改日志状态，0：正常，1：锁定)
	 * @param id
	 */
	public void delete(long id);
	
	/**
	 * 批量删除日志
	 * @param ids 日志ID数组(List<Long>)
	 */
	public void batchDelete(List<Long> ids);
}
