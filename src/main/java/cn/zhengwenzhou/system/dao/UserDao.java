package cn.zhengwenzhou.system.dao;

import cn.zhengwenzhou.system.entity.User;

import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:25
 * @description 用户Dao
 */
public interface UserDao
{
	/**
	 * 获取所有用户
	 * @return List<User>
	 */
	public List<User> getAll();
	
	/**
	 * 根据分页获取用户
	 * @param params 分页参数：pageIndex 起始条数，pageSize 分页大小
	 * @return List<User>
	 */
	public List<User> getListByPage(Map<String, Integer> params);
	
	/**
	 * 根据ID获取用户
	 * @param id 用户ID
	 * @return List<User>
	 */
	public List<User> getUserById(long id);
	
	/**
	 * 根据用户名获取用户
	 * @param username 用户名
	 * @return List<User>
	 */
	public List<User> getUserByUsername(String username);
	
	/**
	 * 检测用户名是否存在
	 * @param username 用户名
	 * @return 0：不存在，>0：存在
	 */
	public int exist(String username);
	
	/**
	 * 获取总条数
	 * @return int
	 */
	public int count();
	
	/**
	 * 添加用户
	 * @param user 用户
	 */
	public void insert(User user);
	
	/**
	 * 更新用户
	 * @param user 用户
	 */
	public void update(User user);
	
	/**
	 * 真实删除用户(不可恢复)
	 * @param id 用户ID
	 */
	public void trueDelete(long id);
	
	/**
	 * 删除用户(更改用户状态，0：正常，1：锁定)
	 * @param id
	 */
	public void delete(long id);
	
	/**
	 * 批量删除用户
	 * @param ids 用户ID数组(List<Long>)
	 */
	public void batchDelete(List<Long> ids);
	
}
