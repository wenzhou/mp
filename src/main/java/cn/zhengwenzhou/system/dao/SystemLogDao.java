package cn.zhengwenzhou.system.dao;

import cn.zhengwenzhou.system.entity.SystemLog;

import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:25
 * @description 日志Dao
 */
public interface SystemLogDao
{
	/**
	 * 获取所有日志
	 * @return List<SystemLog>
	 */
	public List<SystemLog> getAll();
	
	/**
	 * 根据分页获取日志
	 * @param params 分页参数：pageIndex 起始条数，pageSize 分页大小
	 * @return List<SystemLog>
	 */
	public List<SystemLog> getListByPage(Map<String, Integer> params);
	
	/**
	 * 根据ID获取日志
	 * @param id 日志ID
	 * @return List<SystemLog>
	 */
	public List<SystemLog> getSystemLogById(long id);
	
	/**
	 * 获取总条数
	 * @return int
	 */
	public int count();
	
	/**
	 * 添加日志
	 * @param SystemLog 日志
	 */
	public void insert(SystemLog SystemLog);
	
	/**
	 * 删除日志(更改日志状态，0：正常，1：锁定)
	 * @param id
	 */
	public void delete(long id);
	
	/**
	 * 批量删除日志
	 * @param ids 日志ID数组(List<Long>)
	 */
	public void batchDelete(List<Long> ids);
}
