package cn.zhengwenzhou.system.dao.impl;

import cn.zhengwenzhou.system.dao.SystemLogDao;
import cn.zhengwenzhou.system.entity.SystemLog;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:39
 * @description 日志Dao实现
 */
@Repository
public class SystemLogDaoImpl extends SqlSessionDaoSupport implements SystemLogDao
{
	@Resource
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}
	
	@Override
	public List<SystemLog> getAll()
	{
		return this.getSqlSession().selectList("SystemLog.getAll");
	}
	
	@Override
	public List<SystemLog> getListByPage(Map<String, Integer> params)
	{
		return this.getSqlSession().selectList("SystemLog.getListByPage",params);
	}
	
	@Override
	public List<SystemLog> getSystemLogById(long id)
	{
		return this.getSqlSession().selectList("SystemLog.findById",id);
	}
	
	@Override
	public int count()
	{
		return this.getSqlSession().selectOne("SystemLog.count");
	}
	
	@Override
	public void insert(SystemLog SystemLog)
	{
		this.getSqlSession().insert("SystemLog.insert",SystemLog);
	}
	
	@Override
	public void delete(long id)
	{
		this.getSqlSession().delete("SystemLog.delete",id);
	}
	
	@Override
	public void batchDelete(List<Long> ids)
	{
		this.getSqlSession().delete("SystemLog.batchDelete",ids);
	}
}
