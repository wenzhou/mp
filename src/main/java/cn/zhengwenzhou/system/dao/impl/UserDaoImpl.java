package cn.zhengwenzhou.system.dao.impl;

import cn.zhengwenzhou.system.dao.UserDao;
import cn.zhengwenzhou.system.entity.User;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:39
 * @description 用户Dao实现
 */
@Repository
public class UserDaoImpl extends SqlSessionDaoSupport implements UserDao
{
	@Resource
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}
	
	@Override
	public List<User> getAll()
	{
		return this.getSqlSession().selectList("User.getAll");
	}
	
	@Override
	public List<User> getListByPage(Map<String, Integer> params)
	{
		return this.getSqlSession().selectList("User.getListByPage",params);
	}
	
	@Override
	public List<User> getUserById(long id)
	{
		return this.getSqlSession().selectList("User.getUserById",id);
	}
	
	@Override
	public List<User> getUserByUsername(String username)
	{
		return this.getSqlSession().selectList("User.getUserByUsername",username);
	}
	
	@Override
	public int exist(String username)
	{
		return this.getSqlSession().selectOne("User.exist",username);
	}
	
	@Override
	public int count()
	{
		return this.getSqlSession().selectOne("User.count");
	}
	
	@Override
	public void insert(User user)
	{
		this.getSqlSession().insert("User.insert",user);
	}
	
	@Override
	public void update(User user)
	{
		this.getSqlSession().update("User.update",user);
	}
	
	@Override
	public void trueDelete(long id)
	{
		this.getSqlSession().delete("User.trueDelete",id);
	}
	
	@Override
	public void delete(long id)
	{
		this.getSqlSession().update("User.delete",id);
	}
	
	@Override
	public void batchDelete(List<Long> ids)
	{
		this.getSqlSession().update("User.batchDelete",ids);
	}
	
}
