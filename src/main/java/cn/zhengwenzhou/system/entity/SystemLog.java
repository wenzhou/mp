package cn.zhengwenzhou.system.entity;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/29 23:10
 * @description 系统日志实体
 */
public class SystemLog
{
	private long id;
	private long userId;
	private String username;
	private String ip;
	private String method;
	private String params;
	private String status;
	private String returnValue;
	private String exceptionCode;
	private String exceptionDetail;
	private long createTime;
	//自定义注解
	private String annotationModule;
	private String annotationMethod;
	private String annotationDesc;
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public long getUserId()
	{
		return userId;
	}
	
	public void setUserId(long userId)
	{
		this.userId = userId;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public String getIp()
	{
		return ip;
	}
	
	public void setIp(String ip)
	{
		this.ip = ip;
	}
	
	public String getMethod()
	{
		return method;
	}
	
	public void setMethod(String method)
	{
		this.method = method;
	}
	
	public String getParams()
	{
		return params;
	}
	
	public void setParams(String params)
	{
		this.params = params;
	}
	
	public String getStatus()
	{
		return status;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public String getReturnValue()
	{
		return returnValue;
	}
	
	public void setReturnValue(String returnValue)
	{
		this.returnValue = returnValue;
	}
	
	public String getExceptionCode()
	{
		return exceptionCode;
	}
	
	public void setExceptionCode(String exceptionCode)
	{
		this.exceptionCode = exceptionCode;
	}
	
	public String getExceptionDetail()
	{
		return exceptionDetail;
	}
	
	public void setExceptionDetail(String exceptionDetail)
	{
		this.exceptionDetail = exceptionDetail;
	}
	
	public long getCreateTime()
	{
		return createTime;
	}
	
	public void setCreateTime(long createTime)
	{
		this.createTime = createTime;
	}
	
	public String getAnnotationModule()
	{
		return annotationModule;
	}
	
	public void setAnnotationModule(String annotationModule)
	{
		this.annotationModule = annotationModule;
	}
	
	public String getAnnotationMethod()
	{
		return annotationMethod;
	}
	
	public void setAnnotationMethod(String annotationMethod)
	{
		this.annotationMethod = annotationMethod;
	}
	
	public String getAnnotationDesc()
	{
		return annotationDesc;
	}
	
	public void setAnnotationDesc(String annotationDesc)
	{
		this.annotationDesc = annotationDesc;
	}
	
	@Override
	public String toString()
	{
		return "SystemLog{" + "id=" + id + ", userId=" + userId + ", username='" + username + '\'' + ", ip='" + ip + '\'' + ", method='" + method + '\'' + ", params='" + params + '\'' + ", status='" + status + '\'' + ", returnValue='" + returnValue + '\'' + ", exceptionCode='" + exceptionCode + '\'' + ", exceptionDetail='" + exceptionDetail + '\'' + ", createTime=" + createTime + ", annotationModule='" + annotationModule + '\'' + ", annotationMethod='" + annotationMethod + '\'' + ", annotationDesc='" + annotationDesc + '\'' + '}';
	}
}
