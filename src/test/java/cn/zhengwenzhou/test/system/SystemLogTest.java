package cn.zhengwenzhou.test.system;

import cn.zhengwenzhou.system.entity.SystemLog;
import cn.zhengwenzhou.system.service.SystemLogService;
import cn.zhengwenzhou.system.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wen
 * @version 1.0
 * @date 2017/07/30 12:57
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class SystemLogTest
{
	@Autowired
	private UserService userServiceImpl;
	
	@Autowired
	private SystemLogService systemLogServiceImpl;
	
	@Test
	public void test()
	{
		SystemLog systemLog = new SystemLog();
		systemLog.setAnnotationModule("执行方法异常:-->");
		systemLog.setAnnotationMethod("执行方法异常:-->1");
		systemLog.setExceptionDetail("错误信息");
		systemLog.setParams("参数");
		systemLog.setUserId(1);
		systemLog.setUsername("用户名");
		systemLog.setIp("127.0.0.1");
		systemLog.setCreateTime(new Date().getTime());
		systemLogServiceImpl.insert(systemLog);
	}
	
	@Test
	public void t()
	{
		long t = new Long("1501396728438");
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.print(" =================== "+dateFormater.format(t));
	}
}
