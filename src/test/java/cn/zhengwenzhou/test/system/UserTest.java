package cn.zhengwenzhou.test.system;

import cn.zhengwenzhou.system.annotation.SysLog;
import cn.zhengwenzhou.system.entity.User;
import cn.zhengwenzhou.system.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;


/**
 * @author wen
 * @version 1.0
 * @date 2017/07/30 10:39
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"classpath*:applicationContext.xml"})
public class UserTest
{
	@Autowired
	private UserService userServiceImpl;
	
	@Test
	public void getAll()
	{
		List<User> users = userServiceImpl.getAll();
		int a = 9/0;
	}
	
	@Test
	public void getListByPage()
	{
		List<User> users = userServiceImpl.getListByPage(1, 4);
		System.out.println("用户信息：");
		System.out.println(users.toString());
	}
	
	@Test
	public void getUserById()
	{
		User user = userServiceImpl.getUserById(1);
		System.out.println(user.toString());
	}
	
	@Test
	public void getUserByUsername()
	{
		User user = userServiceImpl.getUserByUsername("admin");
		System.out.println(user.toString());
	}
	
	@Test
	public void isExist()
	{
		System.out.println("是否存在："+userServiceImpl.exist("admin1"));
	}
	
	@Test
	public void count()
	{
		System.out.println("总条数："+userServiceImpl.count());
	}
	
	@Test
	public void insert()
	{
		User user = new User();
		user.setUsername("azxc");
		user.setPassword("123456");
		user.setNickname("管理员");
		userServiceImpl.insert(user);
	}
	
	@Test
	public void update()
	{
		User user = userServiceImpl.getUserByUsername("zxcawe");
		User newUser = new User();
		newUser.setId(user.getId());
		newUser.setUsername("新用户");
		newUser.setPassword("123456");
		newUser.setNickname("管理员");
		userServiceImpl.update(newUser);
	}
	
	@Test
	public void trueDelete()
	{
		userServiceImpl.trueDelete(4);
	}
	
	@Test
	public void delete()
	{
		userServiceImpl.delete(5);
	}
	
	@Test
	public void batchDelete()
	{
		List<Long> ids = new ArrayList<>();
		ids.add(new Long(1));
		ids.add(new Long(2));
		ids.add(new Long(3));
		userServiceImpl.batchDelete(ids);
	}
	
	@Test
	@SysLog(module = "测试模块", methods = "T", description = "有用户嘛？")
	public void t()
	{
		int a = 1;
	}
}
